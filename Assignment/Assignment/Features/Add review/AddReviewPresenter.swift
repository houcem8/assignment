import Foundation

class AddReviewPresenter {
    var coordinator: AddReviewCoordinator?
    weak var view: AddReviewViewController?
    
    private let product: Product
    private var rating: Int?
    private var text: String?
    private var reviewsClient: ReviewsClient
    
    init(product: Product, reviewsClient: ReviewsClient) {
        self.product = product
        self.reviewsClient = reviewsClient
    }
    
    func setRating(rating: Int) {
        self.rating = rating
    }
    
    func setText(text: String?) {
        self.text = text
    }
    
    func sendReview() {
        let review = Review(productId: product.productId,
                            rating: rating,
                            text: text,
                            locale: Locale.current.identifier)
        reviewsClient.addReview(review: review) { [weak self] (result) in
            switch result {
            case .success :
                self?.view?.dismiss(animated: true, completion: nil)
            case .failure(let error):
                self?.view?.showError(error: error)
                print("LOG: \(error)")
            }
        }
    }
}
