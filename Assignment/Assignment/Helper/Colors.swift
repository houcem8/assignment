import Foundation
import UIKit

class Colors {
    static let accessoriesColor = UIColor(named: "accessoriesColor")
    static let backgroundColor = UIColor(named: "backgroundColor")
    static let subTitleColor = UIColor(named: "subTitleColor")
    static let titleColor = UIColor(named: "titleColor")
}
