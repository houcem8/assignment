import Foundation

struct EndPoint {
    enum Error: Swift.Error {
        case invalidUrl
        case noInternetConnection
        case invalidResponse
        case noData
        
        func getErrorImageName() -> String {
            switch self {
            case .noData:
                return "no-data"
            default:
                return "network-error"
            }
        }
    }
    
    let path: String
    let queryItems: [URLQueryItem]
    let port: Int
}

extension EndPoint {
    var cummomQueryItems: [URLQueryItem] {
        return [URLQueryItem(name: "accept", value: "application/json")]
    }
    
    var url: URL? {
        var components = URLComponents()
        components.scheme = "http"
        components.host = "localhost"
        components.path = path
        components.queryItems = queryItems + cummomQueryItems
        components.port = port
        return components.url
    }
}
