import Foundation

enum ViewState<T> {
    case error(error: EndPoint.Error)
    case notSet
    case loading
    case hasData(T)
}
