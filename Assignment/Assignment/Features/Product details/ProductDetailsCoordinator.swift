import Foundation
import UIKit

class ProductDetailsCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    private var navigationController : UINavigationController
    private let product: Product
    
    init(navigationController : UINavigationController, product: Product) {
        self.navigationController = navigationController
        self.product = product
    }
    
    func start() {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "ProductDetailsViewController") as? ProductDetailsViewController else {
            return
        }
        let presenter = ProductDetailsPresenter(product: product,
                                                    reviewsClient: Current.reviewsClient)
        presenter.view = viewController
        viewController.presenter = presenter
        viewController.presenter?.coordinator = self
        navigationController.navigationBar.isHidden = false
        navigationController.navigationBar.tintColor = Colors.titleColor
        self.navigationController.pushViewController(viewController, animated: true)
    }
    
    func addReview() {
        let addReviewCoordinator = AddReviewCoordinator(navigationController: navigationController, product: product)
        addReviewCoordinator.start()
        childCoordinators.append(addReviewCoordinator)
    }
}
