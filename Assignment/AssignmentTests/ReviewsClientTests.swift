import XCTest
@testable import Assignment

class ReviewsClientTests: XCTestCase {
    
    var reviewsClient: ReviewsClient!
    
    override func setUp() {
        super.setUp()
        
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [AssignmentTests.MockURLProtocol.self]
        let urlSession = URLSession(configuration: configuration)
        
        reviewsClient = ReviewsClient(session: urlSession)
    }
    
    func testGetReviews_Success() throws {
        let sampleReviews = [
            Review(productId: "H1",
                   rating: 4,
                   text: "Nice product",
                   locale: "en"),
            Review(productId: "H1",
                   rating: 5,
                   text: "I love it!",
                   locale: "fr_FR")
        ]
        let mockData = try JSONEncoder().encode(sampleReviews)
        
        MockURLProtocol.requestHandler = { request in
            return (HTTPURLResponse(), mockData)
        }
        
        // Set expectation. Used to test async code.
        let expectation = XCTestExpectation(description: "Get Reviews")
        
        reviewsClient.getReviews(productId: "H1") { result in
            switch result {
            case .success(let reviews):
                XCTAssertEqual( reviews.count, sampleReviews.count)
                XCTAssertEqual( reviews.first?.productId, sampleReviews.first?.productId)
                XCTAssertEqual( reviews.first?.text, sampleReviews.first?.text)
                XCTAssertEqual( reviews.last?.rating, sampleReviews.last?.rating)
            case .failure(_):
                XCTFail()
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
    }
    
    func testGetProducts_Failed() throws {
        let mockData = "".data(using: .utf8)!
        MockURLProtocol.requestHandler = { request in
            return (HTTPURLResponse(), mockData)
        }
        
        // Set expectation. Used to test async code.
        let expectation = XCTestExpectation(description: "Get Reviews")
        
        reviewsClient.getReviews(productId: "H1") { result in
            switch result {
            case .success(_):
                XCTFail()
            case .failure(let error):
                XCTAssertEqual(error, EndPoint.Error.invalidResponse)
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 0.5)
    }
    
    func testAddReview_Success() throws {
        let sampleReview =
            Review(productId: "H1",
                   rating: 4,
                   text: "Awesome!!",
                   locale: "en")
        
        let mockData = try JSONEncoder().encode(sampleReview)
        
        MockURLProtocol.requestHandler = { request in
            return (HTTPURLResponse(), mockData)
        }
        
        // Set expectation. Used to test async code.
        let expectation = XCTestExpectation(description: "Add Review")
        
        reviewsClient.addReview(review: sampleReview) { result in
            switch result {
            case .success(let review):
                XCTAssertEqual( review?.productId, sampleReview.productId)
                XCTAssertEqual( review?.text, sampleReview.text)
                XCTAssertEqual( review?.rating, sampleReview.rating)
                XCTAssertEqual( review?.locale, sampleReview.locale)
            case .failure(_):
                XCTFail()
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
    }
    
    func testAddReview_withNoRatingAndNoText_Success() throws {
        let sampleReview =
            Review(productId: "H1",
                   rating: nil,
                   text: nil,
                   locale: "en")
        
        let mockData = try JSONEncoder().encode(sampleReview)
        
        MockURLProtocol.requestHandler = { request in
            return (HTTPURLResponse(), mockData)
        }
        
        // Set expectation. Used to test async code.
        let expectation = XCTestExpectation(description: "Add Review")
        
        reviewsClient.addReview(review: sampleReview) { result in
            switch result {
            case .success(let review):
                XCTAssertEqual( review?.productId, sampleReview.productId)
                XCTAssertEqual( review?.text, sampleReview.text)
                XCTAssertEqual( review?.rating, sampleReview.rating)
                XCTAssertEqual( review?.locale, sampleReview.locale)
            case .failure(_):
                XCTFail()
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
    }
    
    func testAddReview_Failed() throws {
        let mockData = "".data(using: .utf8)!
        MockURLProtocol.requestHandler = { request in
            return (HTTPURLResponse(), mockData)
        }
        
        let sampleReview = Review(productId: "H1",
                                  rating: nil,
                                  text: nil,
                                  locale: "en")
        
        // Set expectation. Used to test async code.
        let expectation = XCTestExpectation(description: "Add Review")
        
        reviewsClient.addReview(review: sampleReview) { result in
            switch result {
            case .success(_):
                XCTFail()
            case .failure(let error):
                XCTAssertEqual(error, EndPoint.Error.invalidResponse)
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 0.5)
    }
}
