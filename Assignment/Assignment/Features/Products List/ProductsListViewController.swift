import UIKit

class ProductsListViewController: UIViewController {
    // MARK: - Value Types
    typealias DataSource = UICollectionViewDiffableDataSource<Section, ProductsListCell>

    enum Section: String, CaseIterable {
        case productsListCell
    }
    
    // MARK: - IBOutlet

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchTextFieldLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backButtonLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var presenter: ProductsListPresenter?
    private lazy var dataSource: DataSource = createDataSource()
    
    var viewState: ViewState<[ProductsListCell]> = .notSet {
        didSet {
            setupViewState()
        }
    }
    
    private var isSearchMode = false {
        didSet {
            guard oldValue != isSearchMode else { return }
            animateSearchBar(isSearchMode)
            if isSearchMode {
                presenter?.showSearchedProducts(keyword: searchTextField.text)
            }
        }
    }
    
    // MARK: - Life cycle functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        presenter?.getProducts()
        
        applySnapshot(sections: [], animatingDifferences: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: - private functions

    private func setupViews() {
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionViewCell")
        collectionView.register(UINib(nibName: "ErrorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ErrorCollectionViewCell")
        collectionView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        collectionView.keyboardDismissMode = .onDrag
        
        searchTextField.layer.cornerRadius = 6
        searchTextField.delegate = self
        let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        searchTextField.leftView = leftPaddingView
        searchTextField.leftViewMode = .always
        
        let height = searchTextField.frame.height - 15
        let rightPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: height + 10, height: height))
        
        let searchButton = UIButton(type: .custom)
        searchButton.frame = CGRect(x: 0, y: 0, width: height, height: height)
        searchButton.layer.cornerRadius = 6
        searchButton.backgroundColor = Colors.accessoriesColor
        searchButton.setImage(UIImage(named: "search") ?? UIImage(), for: UIControl.State())
        searchButton.tintColor = .white
        searchButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        searchButton.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
        rightPaddingView.addSubview(searchButton)
        
        searchTextField.rightView = rightPaddingView
        searchTextField.rightViewMode = .always
    }
    
    private func createDataSource() -> DataSource {
        let dataSource = DataSource(collectionView: collectionView,cellProvider: {
            (collectionView, indexPath, productCellType) -> UICollectionViewCell? in
            switch productCellType {
            case .productCell(let product):
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell",for: indexPath) as? ProductCollectionViewCell
                cell?.setupCell(product: product)
                return cell
            case .errorCell(let error):
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ErrorCollectionViewCell",for: indexPath) as? ErrorCollectionViewCell
                cell?.setupCell(error: error)
                return cell
            }
        })
        return dataSource
    }
    
    private func applySnapshot(sections: [ProductsListCell], animatingDifferences: Bool = true) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, ProductsListCell>()
        snapshot.appendSections([Section.productsListCell])
        snapshot.appendItems(sections, toSection: .productsListCell)
        self.dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
    
    private func animateSearchBar(_ isSearchEnabled: Bool) {
        if isSearchEnabled {
            searchTextFieldLeadingConstraint.constant += backButton.frame.width
            backButtonLeadingConstraint.constant -= 10
        } else {
            searchTextFieldLeadingConstraint.constant -= backButton.frame.width
            backButtonLeadingConstraint.constant += 10
        }
        UIViewPropertyAnimator(duration: 0.5, dampingRatio: 0.5, animations: { [weak self] in
            self?.view.layoutIfNeeded()
        }).startAnimation()
    }
    
    private func setupViewState() {
        switch viewState {
        case .hasData(let sections):
            activityIndicator.stopAnimating()
            applySnapshot(sections: sections)
        case .error(let error):
            activityIndicator.stopAnimating()
            applySnapshot(sections: [ProductsListCell.errorCell(error: error)])
        case .loading:
            activityIndicator.startAnimating()
        default:
            break
        }
    }
    
    @objc
    private func searchAction(){
        searchTextField.becomeFirstResponder()
        isSearchMode = true
    }
    
    // MARK: - IBAction functions

    @IBAction func backAction(_ sender: Any) {
        searchTextField.resignFirstResponder()
        searchTextField.text = nil
        isSearchMode = false
        presenter?.showAllProducts()
    }
    
    @IBAction func searchTextFieldDidChange(_ sender: Any) {
        presenter?.showSearchedProducts(keyword: searchTextField.text)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension ProductsListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return presenter?.getCellSize(for: viewState) ?? CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch viewState {
        case .hasData(let cells):
            presenter?.didSelectCell(cell: cells[indexPath.row])
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
}

// MARK: - UITextFieldDelegate

extension ProductsListViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        isSearchMode = true
        return true
    }
}
