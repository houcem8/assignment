import Foundation

struct Product {
  var id: UUID
  var productId: String
  var name: String
  var productDescription: String
  var currency: String
  var price: Float
  var discount: Double?
}

extension Product: Decodable {
  enum CodingKeys: String, CodingKey {
    case productId = "id"
    case name
    case productDescription = "description"
    case currency
    case price
    case discount
  }
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    productId = try container.decode(String.self, forKey: .productId)
    name = try container.decode(String.self, forKey: .name)
    productDescription = try container.decode(String.self, forKey: .productDescription)
    currency = try container.decode(String.self, forKey: .currency)
    price = try container.decode(Float.self, forKey: .price)
    discount = try? container.decode(Double.self, forKey: .discount)

    id = UUID.incrementing()
  }
}

extension Product: Hashable {
  func hash(into hasher: inout Hasher) {
    hasher.combine(id)
  }
  
  static func == (lhs: Product, rhs: Product) -> Bool {
    lhs.id == rhs.id
  }
}

extension Product: Encodable {}
