import UIKit

class ProductDetailsViewController: UIViewController {
    // MARK: - Value Types
    typealias DataSource = UITableViewDiffableDataSource<Section, ProductDetailstCell>

    enum Section: String, CaseIterable {
        case productsCell
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    var presenter: ProductDetailsPresenter?
    private lazy var dataSource: DataSource = createDataSource()
    
    var viewState: ViewState<[ProductDetailstCell]> = .notSet {
        didSet {
            setupViewState()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
        presenter?.getReviews()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    private func setupView() {
        title = presenter?.title
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Review", style: .plain, target: self, action: #selector(addReviewAction))
        navigationItem.rightBarButtonItem?.tintColor = Colors.accessoriesColor
        
        tableView.register(UINib(nibName: "ProductDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductDetailsTableViewCell")
        tableView.register(UINib(nibName: "ReviewTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewTableViewCell")
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
    }
    
    private func createDataSource() -> DataSource {
        let dataSource = DataSource(tableView: tableView) {
            (tableView, indexPath, cellType) -> UITableViewCell? in
            switch cellType {
            case .productInformationCell(let product):
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDetailsTableViewCell", for: indexPath) as! ProductDetailsTableViewCell
                cell.setupCell(product: product)
                return cell
            case .reviewCell(review: let review):
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewTableViewCell", for: indexPath) as! ReviewTableViewCell
                cell.setupCell(review: review)
                return cell
            }
            
        }
        return dataSource
    }
    
    private func applySnapshot(sections: [ProductDetailstCell], animatingDifferences: Bool = true) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, ProductDetailstCell>()
        snapshot.appendSections([Section.productsCell])
        snapshot.appendItems(sections, toSection: .productsCell)
        self.dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
    
    private func setupViewState() {
        switch viewState {
        case .hasData(let sections):
            activityIndicator.stopAnimating()
            applySnapshot(sections: sections)
        case .error:
            activityIndicator.stopAnimating()
        case .loading:
            activityIndicator.startAnimating()
        default:
            break
        }
    }
    
    @objc
    private func addReviewAction(){
        presenter?.addReview()
    }
}
