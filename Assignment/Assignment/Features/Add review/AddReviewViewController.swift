//
//  AddReviewViewController.swift
//  Assignment
//
//  Created by houcem.souid on 15/05/2021.
//

import UIKit

class AddReviewViewController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var reviewTextView: UITextView!
    
    var presenter: AddReviewPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sendButton.layer.cornerRadius = 20
        reviewTextView.delegate = self
    }
    
    func showError(error: EndPoint.Error) {
        let alert = UIAlertController(title: "Error", message: "Please try again later.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func sendAction(_ sender: Any) {
        presenter?.sendReview()
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addReviewAction(_ sender: UIButton) {
        if let value = sender.titleLabel?.text, let rating = Int(value) {
            reviewLabel.text = "Rating: \(rating)"
            presenter?.setRating(rating: rating)
        }else {
            reviewLabel.text = nil
        }
    }
}

extension AddReviewViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
        }
        
        presenter?.setText(text: reviewTextView.text)
        return true
    }
}
