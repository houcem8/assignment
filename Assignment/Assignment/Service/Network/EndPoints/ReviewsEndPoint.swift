import Foundation

extension EndPoint {
    struct Reviews {
        static func addReview(productId: String) -> EndPoint {
            return EndPoint(path: "/reviews/\(productId)",
                            queryItems: [],
                            port: 3002)
        }
        
        static func getReviews(productId: String) -> EndPoint {
            return EndPoint(path: "/reviews/\(productId)",
                            queryItems: [],
                            port: 3002)
        }
    }
}
