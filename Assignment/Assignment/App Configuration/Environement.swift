import Foundation

//This system is implemented in the Kickstarter open source project (https://github.com/kickstarter/ios-oss)
//It help making depency injection easier and more accessible and testable

// wrap all project dependencies in the Environement struct
struct Environment {
    private(set) var productsClient: ProductsClient = .prod
    private(set) var reviewsClient: ReviewsClient = .prod
}

//global varible that should be unique along the project and should be the only access point to all the dependencies
var Current = Environment()
