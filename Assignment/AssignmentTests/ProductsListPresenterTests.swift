import XCTest
@testable import Assignment

class ProductsListPresenterTests: XCTestCase {
    var productsClient: ProductsClient!
    var productsListPresenter: ProductsListPresenter!
    
    override func setUp() {
        super.setUp()
        
        productsClient = ProductsClient()
        productsListPresenter = ProductsListPresenter(productsClient: productsClient)
    }
    
    func testShowSearchedProducts_withNoKeyword() {
        productsListPresenter.showSearchedProducts(keyword: nil)
        XCTAssertEqual(productsListPresenter.dataSource, [])
    }
}
