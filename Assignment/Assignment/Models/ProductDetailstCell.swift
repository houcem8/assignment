import Foundation

enum ProductDetailstCell: Hashable {
    case productInformationCell(product: Product)
    case reviewCell(review: Review)
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(rawValue)
    }
    
    static func == (lhs: ProductDetailstCell, rhs: ProductDetailstCell) -> Bool {
        lhs.rawValue == rhs.rawValue
    }
    
    private var rawValue : String {
        switch self {
        case .productInformationCell(let product):
            return "aProductInformationCell|\(product.id)"
        case .reviewCell(let review):
            return "anReviewCell|\(review.id)"
        }
    }
}
