import Foundation
import UIKit

class ProductsListCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    private var navigationController : UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "ProductsListViewController") as? ProductsListViewController else {
            return
        }
        let presenter = ProductsListPresenter(productsClient: Current.productsClient)
        presenter.view = viewController
        viewController.presenter = presenter
        viewController.presenter?.coordinator = self            
        self.navigationController.setViewControllers([viewController], animated: false)
    }
    
    func showProductDetails(product: Product) {
        let productDetailsCoordinator = ProductDetailsCoordinator(navigationController: navigationController,product: product)
        productDetailsCoordinator.start()
        childCoordinators.append(productDetailsCoordinator)
    }
}
