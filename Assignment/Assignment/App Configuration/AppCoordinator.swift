import Foundation
import UIKit

protocol Coordinator : AnyObject {
    var childCoordinators : [Coordinator] { get set }
    
    func start()
    func childDidFinish(childCoordinator: Coordinator)
}

class AppCoordinator : Coordinator {
    var childCoordinators : [Coordinator] = []
    private var navigationController : UINavigationController!
    private let window : UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        navigationController = UINavigationController()
        
        navigationController.navigationBar.isHidden = true
        navigationController.navigationBar.isTranslucent = true
        
        let productsListCoordinator = ProductsListCoordinator(navigationController: navigationController)
        childCoordinators.append(productsListCoordinator)
        productsListCoordinator.start()
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}

extension Coordinator {
    func childDidFinish(childCoordinator: Coordinator) {
        if let index = childCoordinators.firstIndex(where: {
            (coordinator: Coordinator) -> Bool in
            childCoordinator === coordinator
        }) {
            childCoordinators.remove(at: index)
        }
    }
}

