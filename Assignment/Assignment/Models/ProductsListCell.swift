import Foundation

enum ProductsListCell: Hashable {
    case productCell(product: Product)
    case errorCell(error: EndPoint.Error)
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(rawValue)
    }
    
    static func == (lhs: ProductsListCell, rhs: ProductsListCell) -> Bool {
        lhs.rawValue == rhs.rawValue
    }
    
    private var rawValue : String {
        switch self {
        case .productCell(let product):
            return "aProductCell|\(product.id)"
        case .errorCell:
            return "anError"
        }
    }
}
