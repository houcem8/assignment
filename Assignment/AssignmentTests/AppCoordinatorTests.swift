//
//  AppCoordinatorTests.swift
//  AssignmentTests
//
//  Created by houcem.souid on 13/05/2021.
//

import XCTest
@testable import Assignment

class AppCoordinatorTests: XCTestCase {
    
    private let window = WindowStub()
    private var coordinator: AppCoordinator!
    
    override func setUp() {
        super.setUp()
        
        coordinator = AppCoordinator(window: window)
    }
        
    func testWindow_isKey() {
        coordinator.start()
        
        XCTAssertTrue(window.makeKeyAndVisibleCalled)
        XCTAssertNotNil(window.rootViewController)
    }
    
    func testProductsList_isTopViewController() {
        coordinator.start()
        
        let topNavigationController = window.rootViewController as? UINavigationController
        let topViewController = topNavigationController?.topViewController
        
        XCTAssertTrue(window.makeKeyAndVisibleCalled)
        XCTAssertNotNil(topNavigationController)
        XCTAssertNotNil(topViewController)
        XCTAssertNotNil(topViewController as? ProductsListViewController)
    }
    
}

private class WindowStub: UIWindow {
    var makeKeyAndVisibleCalled = false
    override func makeKeyAndVisible() {
        makeKeyAndVisibleCalled = true
    }
    
}
