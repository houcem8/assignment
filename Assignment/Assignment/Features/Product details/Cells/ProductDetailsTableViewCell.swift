import UIKit

class ProductDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupCell(product: Product) {
        productNameLabel.text = product.name
        productPriceLabel.text = String(format: "%.2f ", product.price) + product.currency
        productDescriptionLabel.text = product.productDescription
    }
    
}
