import XCTest
@testable import Assignment

class EndPointTests: XCTestCase {

    func testUrlComposition() {
        let path = "/path"
        let queryItems: [URLQueryItem] = [
            URLQueryItem(name: "key1", value: "value1"),
            URLQueryItem(name: "key2", value: "value2"),
            URLQueryItem(name: "key3", value: "value3")
        ]
        let port = 3000
        let endPoint = EndPoint(path: path, queryItems: queryItems, port: port)
        guard let url = endPoint.url else {
            XCTFail()
            return
        }
        let components = URLComponents(url: url, resolvingAgainstBaseURL: false)
        XCTAssertEqual(components?.scheme, "http")
        XCTAssertEqual(components?.host, "localhost")
        XCTAssertEqual(components?.path, path)
        XCTAssertEqual(components?.port, port)
        //Components query = queryItem + accept=application/json
        XCTAssertEqual(components?.queryItems?.count, queryItems.count + 1)
        
        let acceptValue: String? = components?.queryItems?
            .first(where: { $0.name == "accept"})
            .flatMap({ item in
            item.value
        })
        XCTAssertEqual(acceptValue, "application/json")
        
        let key2Value: String? = components?.queryItems?
            .first(where: { $0.name == "key2"})
            .flatMap({ item in
            item.value
        })
        XCTAssertEqual(key2Value, "value2")
    }
    
    func testGetProductsEndPoint() {
        let endpoint = EndPoint.Products.getAllProducts()
        XCTAssertEqual(endpoint.path, "/product")
        XCTAssertEqual(endpoint.port, 3001)
    }
    
    func testGetReviewsEndPoint() {
        let product = Product(id: UUID.incrementing(),
                              productId: "H11",
                              name: "Predator",
                              productDescription: "Shoes",
                              currency: "$",
                              price: 59.99)
        
        let endpoint = EndPoint.Reviews.getReviews(productId: product.productId)
        XCTAssertEqual(endpoint.path, "/reviews/\(product.productId)")
        XCTAssertEqual(endpoint.port, 3002)
    }
    
    func testAddReviewEndPoint() {
        let product = Product(id: UUID.incrementing(),
                              productId: "H11",
                              name: "Predator",
                              productDescription: "Shoes",
                              currency: "$",
                              price: 59.99)
        
        let endpoint = EndPoint.Reviews.addReview(productId: product.productId)
        XCTAssertEqual(endpoint.path, "/reviews/\(product.productId)")
        XCTAssertEqual(endpoint.port, 3002)
    }
}
