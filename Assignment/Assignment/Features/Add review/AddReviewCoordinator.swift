import Foundation
import UIKit

class AddReviewCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    private var navigationController : UINavigationController
    private let product: Product
    
    init(navigationController : UINavigationController, product: Product) {
        self.navigationController = navigationController
        self.product = product
    }
    
    func start() {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "AddReviewViewController") as? AddReviewViewController else {
            return
        }
        let presenter = AddReviewPresenter(product: product,
                                           reviewsClient: Current.reviewsClient)
        presenter.view = viewController
        viewController.presenter = presenter
        viewController.presenter?.coordinator = self
        viewController.modalPresentationStyle = .fullScreen
        self.navigationController.present(viewController, animated: true, completion: nil)
    }
}
