import UIKit

class ErrorCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var errorImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCell(error: EndPoint.Error) {
        let errorImage = UIImage(named: error.getErrorImageName())
        errorImageView.image = errorImage
    }

}
