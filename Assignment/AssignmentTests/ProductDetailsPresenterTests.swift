import XCTest
@testable import Assignment

class ProductDetailsPresenterTests: XCTestCase {
    var productDetailsPresenter: ProductDetailsPresenter!
    let product = Product(id: UUID.incrementing(),
                          productId: "H1",
                          name: "prod1",
                          productDescription: "description",
                          currency: "$",
                          price: 20)
        
    override func setUp() {
        super.setUp()
        
        productDetailsPresenter = ProductDetailsPresenter(product: product,
                                                                reviewsClient: ReviewsClient())
    }
    
    func testProductPriceFormat() {
        XCTAssertEqual(productDetailsPresenter.productPrice, "20.00 $")
    }
}
