import Foundation
import UIKit

class ProductsListPresenter {
    var coordinator: ProductsListCoordinator?
    weak var view: ProductsListViewController?
    var dataSource: [Product] = []

    private let productsClient: ProductsClient
    private var allPorducts: [Product] = []
    
    init(productsClient: ProductsClient) {
        self.productsClient = productsClient
    }
    
    func getProducts() {
        view?.viewState = .loading
        productsClient.getAllProducts { [weak self] (result) in
            switch result {
            case .success(let products):
                self?.allPorducts = products
                self?.showAllProducts()
            case .failure(let error):
                self?.setupViewState(error: error)
            }
        }
    }
    
    func showSearchedProducts(keyword: String?) {
        if let keyword = keyword?.uppercased(), !keyword.isEmpty {
            dataSource = allPorducts.filter({ (product) -> Bool in
                product.productId.uppercased().contains(keyword) || product.name.uppercased().contains(keyword)
            })
        }else {
            dataSource = []
        }
        setupViewState()
    }
    
    func showAllProducts() {
        dataSource = allPorducts
        setupViewState()
    }
    
    func getCellSize(for state: ViewState<[ProductsListCell]>) -> CGSize {
        switch state {
        case .error:
            return CGSize(width: UIScreen.main.bounds.width,
                            height: UIScreen.main.bounds.width)
        default:
            return CGSize(width: UIScreen.main.bounds.width/2,
                            height: UIScreen.main.bounds.width/2)
        }
    }
    
    func didSelectCell(cell: ProductsListCell) {
        switch cell {
        case .productCell(product: let product):
            coordinator?.showProductDetails(product: product)
        default:
            break
        }
    }
    
    private func setupViewState(error: EndPoint.Error? = nil) {
        guard error == nil else {
            view?.viewState = .error(error: error!)
            return
        }
        let productsCells = dataSource.map {
            ProductsListCell.productCell(product: $0)
        }
        if productsCells.count == 0 {
            view?.viewState = .error(error: EndPoint.Error.noData)
        }else {
            view?.viewState = .hasData(productsCells)
        }
    }
}
