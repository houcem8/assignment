import Foundation

class ProductsClient {
    static let prod = ProductsClient()
    
    private let networkService: NetworkDataFetcher 
    
    init(session: NetworkSession = URLSession.shared) {
        networkService = NetworkDataFetcher(session: session)
    }
    
    func getAllProducts(completion: @escaping (Result<[Product], EndPoint.Error>) -> Void) -> Void {
        guard let url = EndPoint.Products.getAllProducts().url  else {
            completion(.failure(.invalidUrl))
            return
        }
        let request: URLRequest = URLRequest(url: url)
        networkService.request(request: request) { response in
            let result: Result<[Product], EndPoint.Error>
            defer {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            switch response {
            case .success(let data):
                do{
                    let productsResult = try JSONDecoder().decode([Product].self, from: data)
                    result = .success(productsResult)
                } catch {
                    result = .failure(.invalidResponse)
                }
            case .failure:
                result = .failure(.invalidResponse)
                break
            }
        }
    }
}
