import XCTest
@testable import Assignment

class ProductsClientTests: XCTestCase {
    var productsClient: ProductsClient!
    
    override func setUp() {
        super.setUp()
        
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [AssignmentTests.MockURLProtocol.self]
        let urlSession = URLSession(configuration: configuration)
        
        productsClient = ProductsClient(session: urlSession)
    }
    
    func testGetProducts_Success() throws {
        let sampleProducts = [
            Product(id: UUID.incrementing(),
                    productId: "H1",
                    name: "prod1",
                    productDescription: "description",
                    currency: "$",
                    price: 20),
            Product(id: UUID.incrementing(),
                    productId: "H2",
                    name: "prod2",
                    productDescription: "description",
                    currency: "$",
                    price: 70),
            Product(id: UUID.incrementing(),
                    productId: "H3",
                    name: "prod3",
                    productDescription: "description",
                    currency: "$",
                    price: 120),
        ]
        let mockData = try JSONEncoder().encode(sampleProducts)

        MockURLProtocol.requestHandler = { request in
            return (HTTPURLResponse(), mockData)
        }
        
        // Set expectation. Used to test async code.
        let expectation = XCTestExpectation(description: "GetProducts")

        productsClient.getAllProducts { result in
            switch result {
            case .success(let products):
                XCTAssertEqual( products.count, sampleProducts.count)
                XCTAssertEqual( products.first?.productId, sampleProducts.first?.productId)
                XCTAssertEqual( products.last?.price, sampleProducts.last?.price)
            case .failure(_):
                XCTFail()
            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 0.5)
    }
    
    func testGetProducts_Failed() throws {
        let mockData = "".data(using: .utf8)!
        MockURLProtocol.requestHandler = { request in
            return (HTTPURLResponse(), mockData)
        }
        
        // Set expectation. Used to test async code.
        let expectation = XCTestExpectation(description: "GetProducts")
        
        productsClient.getAllProducts { result in
            switch result {
            case .success(_):
                XCTFail()
            case .failure(let error):
                XCTAssertEqual(error, EndPoint.Error.invalidResponse)
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 0.5)
    }
}
