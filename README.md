# iOS developer assignement

This app is an assignment challange for iOS developer

# Configuration
- Clone the git repo `https://houcem8@bitbucket.org/houcem8/assignment.git`
- Install Docker
- Clone the git repo `git clone https://bitbucket.org/adichallenge/product-reviews-docker-composer.git`
- run command `docker-compose up`
- Use Xcode version 12 or later
- Open the project using the the file `Assignment.xcodeproj`

# Fonctionalities
- A user can see all the products provided in the app.
- A user can search for products by clicking the textField on the top, he can search by product name or product id.
- A user can go from searchng a product to viewing all the available products by pressing the back button
- A user can see the details of a product including all the reviews on that product.
- A user can add a review to a product

# Dependency Injection
I used the Current/Environment approach to manage dependencies and centralize them in one instance 'Current', this was used in the Kickstarter open source project and it's very powerful for mocking and testing.
Example: https://twitter.com/pointfreeco/status/999265422989037573

# Architecture
The implemented architecture is MVP with a coordinator pattern to seperation the navigation logic.
I also used a viewState attribut in the view layer, to represent a single source of truth that the presenter can change in order to refresh the view.

# Unit tests
I focused on adding unit tests especially for the Networking layer.
For that I used the `MockURLProtocol` that helps testing URLSession responses
https://developer.apple.com/videos/play/wwdc2018/417/