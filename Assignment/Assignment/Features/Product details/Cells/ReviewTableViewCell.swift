//
//  ReviewTableViewCell.swift
//  Assignment
//
//  Created by houcem.souid on 15/05/2021.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCell(review: Review) {
        reviewLabel.text = review.text
        if let rating = review.rating {
            ratingLabel.text = "\(rating)"
        }
    }
}
