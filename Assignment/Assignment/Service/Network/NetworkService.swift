import Foundation

// MARK: - Result type
typealias NetworkResult = Result<Data, EndPoint.Error>

// MARK: - Protocol
protocol NetworkService {
    func request(request: URLRequest, completion: @escaping (NetworkResult) -> Void)
}

// MARK: - Request method
enum Method: String {
    case GET
    case POST
    case PUT
    case DELETE
}

// MARK: - Implementation
final class NetworkDataFetcher: NetworkService {
    
    private let session: NetworkSession
    
    init(session: NetworkSession = URLSession.shared) {
        self.session = session
    }
    
    func request(request: URLRequest, completion: @escaping (NetworkResult) -> Void) {
        guard let _ = request.url else {
            completion(.failure(.invalidUrl))
            return
        }
        
        session.loadData(from: request) { (data, response, error) in
            let result: NetworkResult
            defer {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            guard error == nil,
                let response = response as? HTTPURLResponse, (200 ... 299).contains(response.statusCode),
                let data = data else {
                    result = .failure(.invalidResponse)
                    return
            }
            result = .success(data)
        }
    }
}

protocol NetworkSession {
    func loadData(from request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void)
}

extension URLSession: NetworkSession {
    
    func loadData(from request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let task = dataTask(with: request) { (data, response, error) in
            completion(data, response, error)
        }
        task.resume()
    }
}

