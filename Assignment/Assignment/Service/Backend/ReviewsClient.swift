import Foundation

class ReviewsClient {
    static let prod = ReviewsClient()

    private let networkService: NetworkDataFetcher
    
    init(session: NetworkSession = URLSession.shared) {
        networkService = NetworkDataFetcher(session: session)
    }
    
    func getReviews(productId: String, completion: @escaping (Result<[Review], EndPoint.Error>) -> Void) -> Void {
        guard let url = EndPoint.Reviews.getReviews(productId: productId).url  else {
            completion(.failure(.invalidUrl))
            return
        }
        let request: URLRequest = URLRequest(url: url)
        networkService.request(request: request) { response in
            let result: Result<[Review], EndPoint.Error>
            defer {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            switch response {
            case .success(let data):
                do{
                    let reviewsResult = try JSONDecoder().decode([Review].self, from: data)
                    result = .success(reviewsResult)
                } catch {
                    result = .failure(.invalidResponse)
                }
            case .failure:
                result = .failure(.invalidResponse)
                break
            }
        }
    }
    
    func addReview(review: Review, completion: @escaping (Result<Review?, EndPoint.Error>) -> Void) -> Void {
        guard let url = EndPoint.Reviews.addReview(productId: review.productId).url else {
            completion(.failure(.invalidUrl))
            return
        }
        var request: URLRequest = URLRequest(url: url)
        request.httpMethod = Method.POST.rawValue
        request.httpBody = review.data
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("*/*", forHTTPHeaderField: "Accept")
        
        networkService.request(request: request) { response in
            let result: Result<Review?, EndPoint.Error>
            defer {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            switch response {
            case .success(let data):
                do{
                    let reviewResult = try JSONDecoder().decode(Review.self, from: data)
                    result = .success(reviewResult)
                } catch {
                    result = .failure(.invalidResponse)
                }
            case .failure:
                result = .failure(.invalidResponse)
                break
            }
        }
    }
}
