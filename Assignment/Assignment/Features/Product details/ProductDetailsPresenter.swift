import Foundation

class ProductDetailsPresenter {

    private let product: Product
    private let reviewsClient: ReviewsClient
    
    var coordinator: ProductDetailsCoordinator?
    weak var view: ProductDetailsViewController?
    
    var title: String {
        product.productId
    }
    
    var productName: String {
        product.name.uppercased()
    }
    
    var productPrice: String {
        String(format: "%.2f ", product.price) + product.currency
    }
    
    var productDescription: String {
        product.productDescription
    }
    
    init(product: Product, reviewsClient: ReviewsClient) {
        self.product = product
        self.reviewsClient = reviewsClient
    }
    
    func addReview() {
        coordinator?.addReview()
    }
    
    func viewDidLoad() {
        view?.viewState = .hasData([ProductDetailstCell.productInformationCell(product: product)])
    }
    
    func getReviews() {
        view?.viewState = .loading
        reviewsClient.getReviews(productId: product.productId) { result in
            switch result {
            case .success(let reviews):
                let productInformationCell = [ProductDetailstCell.productInformationCell(product: self.product)]
                let nonEmptyReviews = reviews.filter { review in
                    !(review.text?.isEmpty ?? true)
                }
                let reviewsCells = nonEmptyReviews.map { review in
                    return ProductDetailstCell.reviewCell(review: review)
                }
                self.view?.viewState = .hasData(productInformationCell + reviewsCells)

            case .failure(let error):
                print("LOG: \(error)")
                break
            }
        }
    }
}
