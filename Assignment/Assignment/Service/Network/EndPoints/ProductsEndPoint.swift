import Foundation

extension EndPoint {
    struct Products {        
        static func getAllProducts() -> EndPoint {
            return EndPoint(path: "/product",
                            queryItems: [],
                            port: 3001)
        }
    }
}

