//
//  SceneDelegate.swift
//  Assignment
//
//  Created by houcem.souid on 13/05/2021.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var appCoordinator : AppCoordinator?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let appWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
        appWindow.windowScene = windowScene
        window = appWindow
        
        appCoordinator = AppCoordinator(window: appWindow)
        appCoordinator?.start()
    }

}

