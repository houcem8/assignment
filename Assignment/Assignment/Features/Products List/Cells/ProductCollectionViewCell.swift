import UIKit

class ProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!

    var product: Product?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        containerView.layer.cornerRadius = 14
    }
    
    func setupCell(product: Product) {
        self.product = product
        nameLabel.text = product.name + " " + product.productId
        descriptionLabel.text = product.productDescription
      
        var price = product.price
        if let discountValue = product.discount {
          discountLabel.text = "\(discountValue * 100)"
          price = product.price * Float(discountValue)/100
        }else {
          discountLabel.text = nil
        }
        priceLabel.text = String(format: "%.2f ", price) + product.currency
    }

}
