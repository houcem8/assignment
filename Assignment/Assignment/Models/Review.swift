import Foundation

struct Review: Decodable {
    var id: UUID
    let productId: String, rating: Int?, text: String?, locale: String

    var data: Data? {
        var dictionary: [String: Any] = ["productId": productId, "locale": locale]
        if let rating = rating {
            dictionary["rating"] = rating
        }
        if let text = text {
            dictionary["text"] = text
        }
        do {
            return try JSONSerialization.data(withJSONObject: dictionary)
        } catch {
            return nil
        }
    }
    
    enum Keys: String, CodingKey {
        case productId
        case rating
        case text
        case locale
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Keys.self)
        try container.encode(productId, forKey: .productId)
        try container.encode(rating, forKey: .rating)
        try container.encode(text, forKey: .text)
        try container.encode(locale, forKey: .locale)
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        productId = try container.decode(String.self, forKey: .productId)
        locale = try container.decode(String.self, forKey: .locale)
        id = UUID.incrementing()

        do {
            rating = try container.decode(Int.self, forKey: .rating)
        } catch {
            rating = nil
        }
        do {
            text = try container.decode(String.self, forKey: .text)
        } catch {
            text = nil
        }
    }
    
    init(productId: String, rating: Int?, text: String?, locale: String) {
        self.productId = productId
        self.rating = rating
        self.text = text
        self.locale = locale
        id = UUID.incrementing()
    }
}

extension Review: Encodable {}
